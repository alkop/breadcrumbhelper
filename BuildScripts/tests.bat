@echo off
pushd ".."
setlocal EnableDelayedExpansion

set NUNIT="packages\NUnit.ConsoleRunner.3.2.1\tools\nunit3-console.exe"
set OPENCOVER="packages\OpenCover.4.6.519\tools\OpenCover.Console.exe"
set REPORTGEN="packages\ReportGenerator.2.4.4.0\tools\ReportGenerator.exe"
set REPORTS=bin\Reports
set COVERAGE="%REPORTS%\Coverage.xml"

if not exist %REPORTS% md %REPORTS% 

for %%P in ("."; "Net40"; "Net45"; "PCL-Net45"; "Extensions") do (
	for %%T in ("%%P\bin\Release\*Tests.dll") do (
		set TESTLIBS=!TESTLIBS! %%T
))

echo:
echo:Running tests with coverage into %COVERAGE% . . .
echo:
echo:from assemblies: %TESTLIBS%
echo: 

%OPENCOVER%^
 -register:user^
 -target:%NUNIT%^
 -targetargs:"%TESTLIBS%"^
 -filter:"+[*]* -[*Test*]*"^
 -hideskipped:all^
 -output:%COVERAGE%

echo:
echo:Generating HTML coverage report in "%REPORTS%" . . .
echo: 

%REPORTGEN%^
 -reports:%COVERAGE%^
 -targetdir:%REPORTS%^
 -reporttypes:Html;HtmlSummary;Badges^
 -assemblyfilters:-*Test*^

echo:
echo:Succeeded.
endlocal
popd
