﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BreadCrumbHelper.Core
{
    using Models;
    using Extensions;

    public sealed class BreadCrumbs : ICollection<BreadCrumbItem>
    {
        private static readonly Dictionary<string, BreadCrumbs> State = new Dictionary<string, BreadCrumbs>();

        private readonly List<BreadCrumbItem> crumbs;

        public int Count { get { return crumbs.Count; } }
        public bool IsReadOnly { get { return false; } }

        private BreadCrumbs()
        {
            crumbs = new List<BreadCrumbItem>();
        }

        /// <summary>
        /// Get all added breadcrumbs.
        /// </summary>
        public static BreadCrumbs All
        {
            get
            {
                var sessionId = HttpContext.Current.Session.SessionID;
                State[sessionId] = State.GetValueOrDefault(sessionId) ?? new BreadCrumbs();
                return State[sessionId];
            }
        }

        /// <summary>
        /// Get current breadcrumb.
        /// </summary>
        public static BreadCrumbItem Current
        {
            get
            {
                return All.LastOrDefault() ?? BreadCrumbItem.Empty;
            }
        }

        /// <summary>
        /// Return last breadcrumb URL.
        /// </summary>
        public static string CurrentUrl
        {
            get
            {
                return Current.Url;
            }
        }

        /// <summary>
        /// Return previous breadcrumb URL or null if not exists.
        /// </summary>
        public static string PreviousUrl
        {
            get
            {
                return Current.PrevUrl;
            }
        }

        /// <summary>
        /// Clear all breadcrumbs.
        /// </summary>
        public static void Reset()
        {
            All.Clear();
        }

        /// <summary>
        /// Specify title for last added breadcrumb.
        /// </summary>
        /// <param name="title">New title.</param>
        public static void SetTitle(string title)
        {
            Current.Title = title;
        }

        /// <summary>
        /// Adding new breadcrumb.
        /// </summary>
        public static void Add(string url, string title)
        {
            All.Add(new BreadCrumbItem(url, title));
        }

        public void Add(BreadCrumbItem crumb)
        {
            if (crumb.Url == null) return;
            if (crumbs.Any(c => c.Title == crumb.Title))
            {
                Remove(crumb);
            }
            crumbs.Add(crumb);
            crumb.PrevUrl = Count > 1 ? crumbs[Count - 2].Url : null;
        }

        public bool Remove(BreadCrumbItem crumb)
        {
            int @from = crumbs.IndexOf(crumb);
            int to = crumbs.Count - @from;
            crumbs.RemoveRange(@from, to);
            return to > @from;
        }

        public void Clear()
        {
            crumbs.Clear();
        }

        public bool Contains(BreadCrumbItem item)
        {
            return crumbs.Contains(item);
        }

        public void CopyTo(BreadCrumbItem[] array, int arrayIndex)
        {
            crumbs.CopyTo(array, arrayIndex);
        }

        public IEnumerator<BreadCrumbItem> GetEnumerator()
        {
            return crumbs.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
