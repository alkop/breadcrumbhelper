﻿using System.Web.Mvc;

namespace BreadCrumbHelper.Demo.Controllers
{
    using Core.Attributes;

    [BreadCrumb]
    public class HomeController : Controller
    {
        [BreadCrumb(Title = "Home", Reset = true)]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Step1()
        {
            ViewBag.Title = "Step 1";
            return View();
        }

        public ActionResult Step2()
        {
            return View();
        }

        public ActionResult Step3()
        {
            return View();
        }
    }
}