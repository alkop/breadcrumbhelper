﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System.Web.Mvc;

namespace BreadCrumbHelper.Core.Extensions
{
    using Crumbs = BreadCrumbs;

    public static class HtmlExtensions
    {
        /// <summary>
        /// BreadCrumbs renderer.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper instance that this method extends.</param>
        /// <param name="activeClass">CSS class of current breadcrumb. Default class = 'active' (from bootstrap).</param>
        /// <param name="rootClass">Breadcrumbs container class. By default = 'breadcrumbs'.</param>
        /// <returns>An unordered HTML list of breadcrumbs.</returns>
        public static MvcHtmlString BreadCrumbs(this HtmlHelper htmlHelper, string activeClass = "active", string rootClass = "breadcrumbs")
        {
            var rootBuilder = new TagBuilder("ul");
            rootBuilder.AddCssClass(rootClass);
            foreach (var crumb in Crumbs.All)
            {
                var linkBuilder = new TagBuilder("a");
                linkBuilder.MergeAttribute("href", crumb.Url);
                linkBuilder.SetInnerText(crumb.Title);
                var itemBuilder = new TagBuilder("li");
                itemBuilder.InnerHtml += linkBuilder;
                if (Crumbs.CurrentUrl == crumb.Url)
                {
                    itemBuilder.AddCssClass(activeClass);
                }
                rootBuilder.InnerHtml += itemBuilder;
            }
            return MvcHtmlString.Create(rootBuilder.ToString(TagRenderMode.Normal));
        }
    }
}
