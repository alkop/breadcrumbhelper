# ASP.NET MVC BreadCrumb Helper #

[![Build status][appveyor img]][appveyor]
[![Tests][tests img]][tests]
[![NuGet][nuget img]][nuget]
[![License][license img]][license]

---

### Quick start

1. Install last package from NuGet: `PM> Install-Package BreadCrumbHelper`
2. Use `[BreadCrumbAttribute]` to markup controllers:

        #!C#

	    [BreadCrumb]
	    public class HomeController : Controller
	    {
	        [BreadCrumb(Title = "Home", Reset = true)]
	        public ActionResult Index()
	        {
	            return View();
	        }
	
	        public ActionResult Step1()
	        {
	            ViewBag.Title = "Step 1";
	            return View();
	        }
	
	        public ActionResult Step2()
	        {
	            return View();
	        }
	
	        public ActionResult Step3()
	        {
	            return View();
	        }
	    }
	

	__Title priority__: Attribute level => ViewBag.Title => Action name.

3. Inside your **Layout.cshtml** display breadcrumbs like this:

        #!C#

		@Html.BreadCrumbs(activeClass: "current", rootClass: "breadcrumbs")


4. Enjoy

For additional help see [Wiki Page][WikiHome].

[WikiHome]: https://bitbucket.org/alkop/breadcrumbhelper/wiki/

[appveyor]:https://ci.appveyor.com/project/AlexanderKopylov/breadcrumbhelper/branch/master
[appveyor img]:https://ci.appveyor.com/api/projects/status/oi7de33ikkuwk1r9/branch/master?svg=true

[nuget]:https://www.nuget.org/packages/breadcrumbhelper
[nuget img]:https://img.shields.io/nuget/v/breadcrumbhelper.svg

[tests]:http://alkop.bitbucket.org/breadcrumbs/tests.html
[tests img]:https://img.shields.io/badge/Coverage-60%25-orange.svg

[license]:LICENSE.md
[license img]:https://img.shields.io/badge/license-MIT-blue.svg