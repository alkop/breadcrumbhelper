﻿using System.IO;
using System.Reflection;
using System.Web;
using System.Web.SessionState;
using NUnit.Framework;

namespace BreadCrumbHelper.Tests
{
    using Crumbs = Core.BreadCrumbs;

    public class BreadCrumbTests : Assert
    {
        private Crumbs crumbs;

        private const string HomeUrl = "localhost";
        private const string SettingUrl = HomeUrl + "/setting/get";
        private const string UserUrl = HomeUrl + "/setting/get?Name=John";

        [SetUp]
        public void SetUp()
        {
            var httpRequest = new HttpRequest("", "http://localhost/", "");
            var httpResponce = new HttpResponse(new StringWriter());
            var httpContext = new HttpContext(httpRequest, httpResponce);
            var sessionContainer = new HttpSessionStateContainer("fakeSessionId",
                                                                 new SessionStateItemCollection(),
                                                                 new HttpStaticObjectsCollection(),
                                                                 10,
                                                                 true,
                                                                 HttpCookieMode.AutoDetect,
                                                                 SessionStateMode.InProc,
                                                                 false);
            httpContext.Items["AspSession"] = typeof(HttpSessionState)
                                                .GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance,
                                                                null,
                                                                CallingConventions.Standard,
                                                                new[] { typeof(HttpSessionStateContainer) },
                                                                null)
                                                .Invoke(new object[] { sessionContainer });
            HttpContext.Current = httpContext;

            crumbs = Crumbs.All;
        }

        [Test]
        public void Add()
        {
            for (int i = 0; i < 10; i++)
            {
                Crumbs.Add(HomeUrl, "Home");
            }
            That(crumbs.Count == 1);
            Crumbs.Add(SettingUrl, "Settings");
            That(crumbs.Count == 2);
            That(Crumbs.CurrentUrl == SettingUrl);
            That(Crumbs.PreviousUrl == HomeUrl);
            Crumbs.Add(HomeUrl, "Home");
            That(Crumbs.CurrentUrl == HomeUrl);
            IsNull(Crumbs.PreviousUrl);
            Crumbs.Add(UserUrl, "Users");
            That(crumbs.Count == 2);
            That(Crumbs.PreviousUrl == HomeUrl);
            That(Crumbs.CurrentUrl == UserUrl);
        }

        [Test]
        public void SetTitle()
        {
            var title = "Edit user";
            Crumbs.Add(UserUrl, "Users");
            Crumbs.SetTitle(title);
            That(Crumbs.Current.Title == title);
        }

        [Test]
        public void Reset()
        {
            Crumbs.Reset();
            IsEmpty(crumbs);
        }
    }
}
